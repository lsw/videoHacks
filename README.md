videoHacks
==========

english version at the bottom

La bonne (web)cam qui tourne
----------------------------

mots-clés: video hack DIY open-source

objectif: assembler et faire fonctionner un kit permettant de motoriser une webcam qui pivotera ensuite sur 2 axes: vertical et horizontal (tangage et lacet) via un contrôleur open source.

Prérequis: un peu de patience, de la curiosité, l'envie de réaliser soi-même son outil 

Code source en ligne: https://gitlab.com/lsw/videoHacks

Inspiration: caméras PTZ dites de vidéosurveillance type Axis 214, tourelle de tanks de modélisme

Applications: big brotherisation, performance, VJaying, spectacle vivant, loisirs, babyphone/cam

Liste du matériel
-----------------

* kit pièces à imprimer 3D + visserie OU kit pièces à découper en laser en contreplaqué + visserie
* 2 microservos moteurs à retour d'information type ADA-01449 
* une Webcam USB type Logitech c270 c910 c615 c510 OU une GoPro
* un arduino + cable usb + adaptateur 5v OU un raspberry pi + câble Ethernet + adaptateur 5V + carte microSD (+camera Pi)
* boitier pour sa carte à imprimer en PLA ou assembler en découpe laser de contreplaqué ou acrylique
* une installation fonctionelle de Pd-extended, d'Arduino ou de Processing  sur son PC (ça marche aussi sur un Mac :)
 
 en option:
 * son logiciel préféré qui fonctionne avec OSC
 * son interface MIDI préférée
     
outils pour l'atelier
---------------------
* tournevis cruciforme électronique
* presse ou petit étau
* cutter
* écran + HDMI + clavier + souris
* connexion Internet
* vidéo-projecteur

Budget
------

(tarifs arrondis de snootlab.com hors frais de port)

* 2 moteurs micro servo : 22€
* kit raspberry 2 ( Framboise+ microSD 16Go + alim 5V+ cable microUSB): 70€
* kit raspberry 2 + cam : 95€
* kit arduino (Uno + câble USB  + alim 5V) : 35€

Je proposerais sans doute à la vente prochainement un kit de pièces pour la tourelle et le boitier en PLA et/ou en contreplaqué + visserie.

A webcam-that-pans-and-tilts
----------------------------

not much to read as of now

BOM
---


